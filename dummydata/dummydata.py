'''
Creat a dataset of dummy people for testing the research consent database
'''

import csv
from random import choice, choices, randrange
from datetime import datetime
from faker import Faker
import names

OUTFILE_NAME = "dummydata.csv"
OUTFILE_NAME_CONDITIONS = "dummydata_conditions.csv"
OUTFILE_NAME_RECRUITED = "dummydata_recruited.csv"

OUTFILE_HEADERS = ["record_id",
                   "first_name",
                   "lastname",
                   "dob",
                   "gender",
                   "date_recruitment",
                   "email",
                   "address",
                   "postcode",
                   "phone",
                   "ethnicity",
                   "eth_asian",
                   "eth_black",
                   "eth_hispanic",
                   "eth_mixed",
                   "eth_other",
                   "eth_white"]

OUTFILE_HEADERS_RECRUIT = ["recruited"]

with open(OUTFILE_NAME, "w", encoding="UTF-8") as outfile:
    writer = csv.DictWriter(outfile,
                            fieldnames=OUTFILE_HEADERS)
    writer.writeheader()

eth_dict = {1: ["eth_asian", 6],
            2: ["eth_black", 5],
            3: ["eth_hispanic", 3],
            4: ["eth_mixed", 4],
            5: ["eth_other", 2],
            6: ["eth_white", 4]}

specialitylist = {"Speciality__1": "Alzheimer's disease",
                  "Speciality__2": "Generalised epilepsy",
                  "Speciality__3": "Relapsing-remitting multiple sclerosis",
                  "Speciality__4": "Vasculitis",
                  "Speciality__5": "Motor neurone disease",
                  "Speciality__6": "Migraine",
                  "Speciality__7": "Functional neurological disorder",
                  "Speciality__8": "Idiopathic Parkinson's disease",
                  "Speciality__9": "Ataxia",
                  "Speciality__10": "Tuberous sclerosis",
                  "Speciality__11": "MELAS"}
spec_headers = ["record_id"] + list(specialitylist.keys()) + ["diagnosis"]

with open(OUTFILE_NAME_CONDITIONS, "w", encoding="UTF-8") as outfile_recruit:
    writer = csv.DictWriter(outfile_recruit,
                            fieldnames=spec_headers)
    writer.writeheader()

recruit_headers = ["record_id",
                   "recruited"]

with open(OUTFILE_NAME_RECRUITED, "w", encoding="UTF-8") as outfile:
    writer = csv.DictWriter(outfile,
                            fieldnames=recruit_headers)
    writer.writeheader()

# loop through 500 participants
for n in range(500):
    # create name
    lastname = names.get_last_name()
    firstname = names.get_first_name()

    # assign address as Addenbrookes
    ADDRESS = "Addenbrookes hospital, Long road, Cambridge"
    POSTCODE = "CB2 0QQ"

    # assign dummy email address
    EMAIL = "dummy@participant.com"

    # assign phone number
    PHONE = '01234 567891'

    # assign a random speciality and diagnosis
    speciality, diagnosis = choice(list(specialitylist.items()))

    # dummy date of birth
    fake = Faker()
    dob = fake.date_between(start_date='-95y', end_date='-16y')

    # dummy gender
    GENDER = choices([1, 2, 3, 4],
                     weights=[45, 45, 5, 5])[0]

    # dummy date of recruitment
    date_recruitment = fake.date_between(start_date='-5y',
                                         end_date='today')

    # ethnicity
    ethnicity = choices([1, 2, 3, 4, 5, 6],
                        weights=[3, 4, 3, 6, 4, 80])[0]

    sub_eth = eth_dict[ethnicity][0]
    if ethnicity == 6:
        sub_eth_n = choices([1, 2, 3, 4],
                            weights=[91, 3, 3, 3])[0]
    else:
        sub_eth_n = randrange(1, eth_dict[ethnicity][1] + 1)

    # recruitment record
    RECRUITED = choices([0, 1],
                        weights=[75, 25])[0]

    # write out data
    with open(OUTFILE_NAME, "a", encoding="UTF-8") as outfile:
        outdict = {header: "" for header in OUTFILE_HEADERS}
        outdict["record_id"] = str(n + 1)
        outdict["first_name"] = firstname
        outdict["lastname"] = lastname
        outdict["dob"] = datetime.strftime(dob, "%Y-%m-%d")
        outdict["gender"] = GENDER
        outdict["date_recruitment"] = datetime.strftime(date_recruitment,
                                                        "%Y-%m-%d")
        outdict["email"] = EMAIL
        outdict["address"] = ADDRESS
        outdict["postcode"] = POSTCODE
        outdict["phone"] = PHONE
        outdict["ethnicity"] = ethnicity
        outdict[sub_eth] = sub_eth_n

        writer = csv.DictWriter(outfile,
                                fieldnames=OUTFILE_HEADERS)
        writer.writerow(outdict)

    with open(OUTFILE_NAME_CONDITIONS,
              "a",
              encoding="UTF-8") as outfile_specdiag:
        outdict_specdiag = {spec: "" for spec in specialitylist}
        outdict_specdiag["record_id"] = str(n + 1)
        outdict_specdiag[speciality] = "1"
        outdict_specdiag["diagnosis"] = diagnosis

        writer_specdiag = csv.DictWriter(outfile_specdiag,
                                         fieldnames=spec_headers)
        writer_specdiag.writerow(outdict_specdiag)

    with open(OUTFILE_NAME_RECRUITED,
              "a",
              encoding="UTF-8") as outfile_recruit:
        outdict_recruit = {rec: "" for rec in recruit_headers}
        outdict_recruit["record_id"] = str(n+1)
        outdict_recruit["recruited"] = RECRUITED

        writer_recruited = csv.DictWriter(outfile_recruit,
                                          fieldnames=recruit_headers)
        writer_recruited.writerow(outdict_recruit)
